package com.prex.auth.handler;

import cn.hutool.core.collection.CollUtil;
import com.prex.common.auth.service.PrexSecurityUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * @Classname prexAuthenticationSuccessHandler
 * @Description 在验证过程中成功会触发此类事件
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-08-06 15:24
 * @Version 1.0
 */
@Slf4j
@Component
public class PrexAuthenticationSuccessEventHandler implements ApplicationListener<AuthenticationSuccessEvent> {

    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event) {

        Authentication authentication = (Authentication) event.getSource();
        if (CollUtil.isNotEmpty(authentication.getAuthorities())) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof PrexSecurityUser){
                log.info("用户：{} 登录成功",((PrexSecurityUser) principal).getUsername());
            }
        }
    }
}



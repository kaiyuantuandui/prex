package com.prex.base.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.prex.base.api.entity.SysDict;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author lihaodong
 * @since 2019-05-17
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

}
